//
//  ExchangePresenter.swift
//  ExchangeCalculator
//
//  Created by diwka on 14/6/22.
//
import Alamofire
import Foundation
import SwiftyJSON

protocol ExcHangePresenterDelegate: AnyObject {
    func amount(amount: Int)
    func from(from: String)
    func to(to: String)
    func result(result: Double)
    
}

class ExchangePresenter{
    
    func backToColculator() {
        router.back()
    }
    
    weak var delegate: ExcHangePresenterDelegate?
    
    private let router: Router
    
    required init(view: ExcHangePresenterDelegate, router: Router){
        self.delegate = view
        self.router = router
    }
    
    
    let apiKey = "MDD17iHWGYDqFqbiP7BUXv4FnGQPxm9H"
    
    let cuurrentArray = ["USD", "KGS", "RUB", "EUR"]
    
    
    func getPrice(amount: Int?, from: String?, to: String?, result: Double?, succes: Bool?){
        
        let headers: HTTPHeaders = ["apikey": apiKey]
        
        let parametrs = ["amount": amount ?? 0,
                         "from": from ?? "null",
                         "to": to ?? "null",
                         "result": result!,
                         "succes": succes ?? "null"] as [String : Any]
        
        AF.request("https://api.apilayer.com/exchangerates_data/convert?", method: .get, parameters: parametrs, headers: headers).responseJSON {  response in
            
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
                
                let result = "\(json["result"])"
                
                self.delegate?.result(result: (Double(result) ?? 0.0) )
                print(response.result)
                
            case .failure(let error):
                print(error)
            }
        }
        DataBase.shared.saveHistoryExchange(model: HistoryexchangeModel.createHistory(to: to!, from: from!, amount: amount!, result: result!))
    }
    
    func selectExchangeHistory(model: HistoryexchangeModel) {
        delegate?.result(result: model.result)
        delegate?.amount(amount: model.amount)
        delegate?.from(from: model.from)
        delegate?.to(to: model.to)
    }
}

