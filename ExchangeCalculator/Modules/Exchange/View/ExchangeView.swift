
import Foundation
import UIKit
import Alamofire

class ExchangeView: UIViewController {
    
    var presenter: ExchangePresenter!
    
    private lazy var numberTextField: UITextField = {
        let view =  UITextField()
        view.placeholder = "0"
        view.font = .systemFont(ofSize: 20, weight: .regular)
        view.textAlignment = .left
        view.borderStyle = .bezel
        return view
    }()
    
    private lazy var resultLabel: UILabel = {
        let view =  UILabel()
        view.text = ""
        view.font = .systemFont(ofSize: 20, weight: .regular)
        view.textAlignment = .left
        view.textColor = .init(named: "ColorLine")
        return view
    }()
    
    private lazy var toCurrencyTextField: UITextField = {
        let view = UITextField()
        view.text = "USD"
        view.font = .systemFont(ofSize: 20, weight: .regular)
        view.borderStyle = .bezel
        view.textAlignment = .center
        return view
    }()
    
    private lazy var toCurrencyPicker: UIPickerView = {
        let view =  UIPickerView()
        view.dataSource = self
        view.delegate = self
        view.tag = 1
        return view
    }()
    
    private lazy var fromCurrencyTextField: UITextField = {
        let view = UITextField()
        view.text = "KGS"
        view.font = .systemFont(ofSize: 20, weight: .regular)
        view.borderStyle = .bezel
        view.textAlignment = .center
        return view
    }()
    
    private lazy var fromCurrencyPicker: UIPickerView = {
        let view =  UIPickerView()
        view.dataSource = self
        view.delegate = self
        view.tag = 0
        return view
    }()
    
    private lazy var calculatorView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var calculatorline: UIView = {
        let view = UIView()
        view.backgroundColor = .init(named: "ColorLine")
        return view
    }()
    
    private lazy var historyButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage.init(named: "Time Machine"), for: .normal)
        view.addTarget(self, action: #selector(historyExchange(view: )), for: .touchUpInside)
        return view
    }()
    
    private lazy var doneExchangeButton: UIButton = {
        let view = UIButton()
        view.setTitle("готово", for: .normal)
        view.addTarget(self, action: #selector(doneExchangeButton(view:)), for: .touchUpInside)
        view.setTitleColor(UIColor.init(named: "ColorNumber"), for: .normal)
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.init(named: "ColorNumber")?.cgColor
        return view
    }()
    
    private lazy var exitButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage.init(systemName: "chevron.backward"), for: .normal)
        view.tintColor = .init(named: "ColorNumber")
        view.addTarget(self, action: #selector(clickExit(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var toolbarPicker: UIToolbar = {
        let view = UIToolbar()
        view.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(doneDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        view.setItems([spaceButton,doneButton], animated: true)
        return view
    }()
    
    private func createButton(title: String, color: String, backgrounColor: String) -> UIButton {
        let view = UIButton()
        view.setTitle(title, for: .normal)
        view.setTitleColor(UIColor(named: color), for: .normal)
        view.titleLabel?.font = UIFont.systemFont(ofSize: 24, weight: .regular)
        view.layer.shadowRadius = 10
        view.layer.shadowOpacity = 0.05
        view.layer.shadowColor = UIColor.white.cgColor
        view.backgroundColor = .init(named: backgrounColor)
        DispatchQueue.main.async {
            view.layer.cornerRadius = view.frame.width / 9
        }
        return view
    }
    
    @objc func doneDatePicker(view: UIButton){
        self.view.endEditing(true)
    }
    
    @objc func historyExchange(view: UIButton){
        navigationController?.present(HistoryExchangeView(), animated: true)
    }
    
    @objc func doneExchangeButton(view: UIButton){
        let amountCurrency = numberTextField.text
        let resultCurrency = resultLabel.text
        presenter.getPrice(amount: Int(amountCurrency!) ?? 0, from: fromCurrencyTextField.text ?? "", to: toCurrencyTextField.text ?? "", result: (Double(resultCurrency!) ?? 0.0), succes: Bool())
    }
    
    @objc func clickExit(view: UIButton){
        presenter.backToColculator()
    }
    
    override func viewDidLoad() {
        
        view.backgroundColor = .init(named: "ColorNumber-1")
        super.viewDidLoad()
        
        view.addSubview(calculatorView)
        calculatorView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.right.equalTo(view.safeArea.right)
            make.left.equalTo(view.safeArea.left)
            make.height.equalTo(view.frame.height / 2.4)
        }
        
        view.addSubview(exitButton)
        exitButton.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.left.equalToSuperview()
            make.height.width.equalTo(48)
        }
        
        calculatorView.addSubview(fromCurrencyTextField)
        fromCurrencyTextField.snp.makeConstraints { make in
            make.top.equalTo(exitButton.snp.bottom).offset(view.frame.height / 25)
            make.left.equalToSuperview().offset(19)
            make.width.equalTo(view.frame.width / 3)
            fromCurrencyTextField.inputView = fromCurrencyPicker
            fromCurrencyTextField.inputAccessoryView = toolbarPicker
        }
        
        calculatorView.addSubview(toCurrencyTextField)
        toCurrencyTextField.snp.makeConstraints { make in
            make.top.equalTo(fromCurrencyTextField.snp.bottom).offset(view.frame.height / 25)
            make.left.equalToSuperview().offset(19)
            make.width.equalTo(view.frame.width / 3)
            toCurrencyTextField.inputView = toCurrencyPicker
            toCurrencyTextField.inputAccessoryView = toolbarPicker
        }
        
        calculatorView.addSubview(numberTextField)
        numberTextField.snp.makeConstraints { make in
            make.top.equalTo(exitButton.snp.bottom).offset(view.frame.height / 25)
            make.right.equalToSuperview().offset(-19)
            make.width.equalTo(view.frame.width / 3)
        }
        
        calculatorView.addSubview(resultLabel)
        resultLabel.snp.makeConstraints { make in
            make.top.equalTo(numberTextField.snp.bottom).offset(view.frame.height / 25)
            make.right.equalToSuperview().offset(-19)
            make.width.equalTo(view.frame.width / 3)
        }
        
        view.addSubview(historyButton)
        historyButton.snp.makeConstraints { make in
            make.top.equalTo(resultLabel.snp.bottom).offset(view.frame.height / 4)
            make.left.equalToSuperview().offset(19)
            make.width.height.equalTo(24)
        }
        
        view.addSubview(doneExchangeButton)
        doneExchangeButton.snp.makeConstraints { make in
            make.top.equalTo(resultLabel.snp.bottom).offset(view.frame.height / 4)
            make.right.equalToSuperview().offset(-19)
            make.width.equalTo(view.frame.width / 3.5)
            make.height.equalTo(24)
        }
        
        calculatorView.addSubview(calculatorline)
        calculatorline.snp.makeConstraints { make in
            make.top.equalTo(historyButton.snp.bottom).offset((view.frame.height / 28.5) * 1)
            make.left.equalTo(view.safeArea.left).offset((view.frame.width / 28.5) * 1)
            make.right.equalTo(view.safeArea.right).offset((view.frame.width / 28.5) * -1)
            make.height.equalTo(1)
        }
    }
}

extension ExchangeView: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0{
            fromCurrencyTextField.text = presenter.cuurrentArray[row]
            
        }else if pickerView.tag == 1{
            toCurrencyTextField.text = presenter.cuurrentArray[row]
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return presenter.cuurrentArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            let component = presenter.cuurrentArray[row]
            return component
        } else if pickerView.tag == 1 {
            let component = presenter.cuurrentArray[row]
            return component
        } else {
            return ""
        }
    }
}
extension ExchangeView: ExcHangePresenterDelegate, HistoryExchangeSelectDelegate{
    
    func selectExchangeHistory(model: HistoryexchangeModel) {
        presenter.selectExchangeHistory(model: model)
    }
    
    func amount(amount: Int) {
        numberTextField.text = "\(amount)"
    }
    
    func from(from: String) {
        fromCurrencyTextField.text = from
    }
    
    func to(to: String) {
        toCurrencyTextField.text = to
    }
    
    func result(result: Double) {
        resultLabel.text = "\(result)"
    }
}
