//
//  ExchangeModel.swift
//  ExchangeCalculator
//
//  Created by diwka on 14/6/22.
//

import Foundation

struct Converter: Codable {
    let query: Query?
    let result: Double?
    let success: Bool?
}

struct Query: Codable {
    let amount: Int?
    let from, to: String?
}
