//
//  HistoryPresenter.swift
//  ExchangeCalculator
//
//  Created by diwka on 7/6/22.
//

import Foundation
import RealmSwift

protocol SharedHisstory: AnyObject{
    func showHistory(model: Results<HistoryModel>)
}

class HistoryPresenter {
    
    private weak var delegate: SharedHisstory? = nil
    weak var historyDelegate: HistorySelectDelegate? = nil
    
    func selctHistory(model: HistoryModel){
        historyDelegate?.selectHistory(model: model)
    }
    
    func deleteAll(){
        DataBase.shared.deleteAllHistory()
    }
    
    func getHistory() -> Results<HistoryModel>?{
        return DataBase.shared.getHistory()
    }
}
