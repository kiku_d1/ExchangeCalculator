//
//  HistoryCell.swift
//  ExchangeCalculator
//
//  Created by diwka on 7/6/22.
//

import Foundation
import UIKit

class HistoryCell: UITableViewCell {
    
    private lazy var numberLabel: UILabel = {
        let view =  UILabel()
        view.text = "55"
        view.font = .systemFont(ofSize: 28, weight: .regular)
        view.textAlignment = .right
        return view
    }()
    
    private lazy var resultLabel: UILabel = {
        let view =  UILabel()
        view.text = "0"
        view.font = .systemFont(ofSize: 28, weight: .regular)
        view.textAlignment = .right
        view.textColor = .init(named: "ColorLine")
        return view
    }()
    
    override func layoutSubviews() {
        
        addSubview(numberLabel)
        numberLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-19)
            make.left.equalToSuperview()
            make.width.equalTo(48)
        }
        
        addSubview(resultLabel)
        resultLabel.snp.makeConstraints { make in
            make.top.equalTo(numberLabel.snp.bottom).offset(20)
            make.right.equalToSuperview().offset(-19)
            make.left.equalToSuperview()
            make.width.equalTo(48)
        }
    }
    
    func fill(model: HistoryModel){
        numberLabel.text = model.math
        resultLabel.text = model.result
    }
}
