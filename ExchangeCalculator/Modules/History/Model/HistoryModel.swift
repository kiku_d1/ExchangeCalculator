//
//  HistoryModel.swift
//  ExchangeCalculator
//
//  Created by diwka on 7/6/22.
//

import Foundation
import RealmSwift

class HistoryModel: Object {
    
    @objc dynamic var math = ""
    @objc dynamic var result = ""
    
    static func createHistory(math: String, result: String) -> HistoryModel {
        let model = HistoryModel()
        model.math = math
        model.result = result
        return model 
    }
}
