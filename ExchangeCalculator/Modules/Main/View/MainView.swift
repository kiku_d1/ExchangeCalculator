

import UIKit
import SnapKit


class MainView: UIViewController {
    
    var presenter: MainPresenter!
    private lazy var buttonsLayout = UIButtonLayoutView()
    
    private lazy var numberLabel: UILabel = {
        let view =  UILabel()
        view.text = "0"
        view.font = .systemFont(ofSize: 48, weight: .regular)
        view.textAlignment = .right
        return view
    }()
    
    private lazy var resultLabel: UILabel = {
        let view =  UILabel()
        view.text = "0"
        view.font = .systemFont(ofSize: 48, weight: .regular)
        view.textAlignment = .right
        view.textColor = .init(named: "ColorLine" )
        return view
    }()
    
    private lazy var calculatorView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var calculatorline: UIView = {
        let view = UIView()
        view.backgroundColor = .init(named: "ColorLine")
        return view
    }()
    
    private lazy var historyButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage.init(named: "Time Machine"), for: .normal)
        view.addTarget(self, action: #selector(clickHistory(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var exchangeButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage.init(named: "Root"), for: .normal)
        view.addTarget(self, action: #selector(clickExchange(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var backSpaceButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage.init(named: "Backspace"), for: .normal)
        view.addTarget(self, action: #selector(clickBackspace(view:)), for: .touchUpInside)
        return view
    }()

    @objc func clickExchange(view: UIButton){
        presenter.showExchange()
    }
    
    @objc func clickHistory(view: UIButton){
        navigationController?.present(HistoryView(), animated: true)
    }
    
    @objc func clickNumber(view: UIButton){
        presenter.clickButton(view.titleLabel?.text ?? String())
    }
    
    @objc func clickBackspace(view: UIButton){
        presenter.clearLast()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .init(named: "ColorNumber-1")
        
        buttonsLayout.delegate = self
        
        view.addSubview(calculatorView)
        calculatorView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.right.equalTo(view.safeArea.right)
            make.left.equalTo(view.safeArea.left)
            make.height.equalTo(view.frame.height / 4.8)
        }
        
        calculatorView.addSubview(numberLabel)
        numberLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.right.equalToSuperview().offset(-19)
            make.left.equalToSuperview()
            make.width.equalTo(48)
        }
        
        calculatorView.addSubview(resultLabel)
        resultLabel.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.right.equalToSuperview().offset(-19)
            make.left.equalToSuperview()
            make.width.equalTo(48)
        }
        
        view.addSubview(buttonsLayout)
        buttonsLayout.snp.makeConstraints { make in
            make.left.equalTo(view.safeArea.left).offset(view.frame.width / 28)
            make.right.equalTo(view.safeArea.right).offset((view.frame.width / 28) * -1)
            make.bottom.equalTo(view.safeArea.bottom).offset((view.frame.width / 28) * -1)
            make.height.equalTo(((view.frame.width - (view.frame.width / 28)) / 4) * 5)
        }
        
        buttonsLayout.createButtonLayouts(cornerRadius: ((self.view.frame.width / 4.0)))

        view.addSubview(historyButton)
        historyButton.snp.makeConstraints { make in
            make.bottom.equalTo(buttonsLayout.snp.top).offset((view.frame.width / 30) * -2)
            make.left.equalTo(view.safeArea.left).offset(19)
            make.width.height.equalTo(34)
        }
        
        view.addSubview(exchangeButton)
        exchangeButton.snp.makeConstraints { make in
            make.bottom.equalTo(buttonsLayout.snp.top).offset((view.frame.width / 30) * -2)
            make.left.equalTo(historyButton.snp.right).offset(25)
            make.width.height.equalTo(34)
        }
        
        view.addSubview(backSpaceButton)
        backSpaceButton.snp.makeConstraints { make in
            make.bottom.equalTo(buttonsLayout.snp.top).offset((view.frame.width / 30) * -2)
            make.right.equalToSuperview().offset(-19)
            make.width.height.equalTo(34)
        }
        
        calculatorView.addSubview(calculatorline)
        calculatorline.snp.makeConstraints { make in
            make.bottom.equalTo(buttonsLayout.snp.top).offset(-20)
            make.left.equalTo(view.safeArea.left).offset((view.frame.width / 28))
            make.right.equalTo(view.safeArea.right).offset((view.frame.width / 28) * -1)
            make.height.equalTo(1)
        }
    }
}

extension MainView: MainPresenterDelegate, UIButtonLayoutDelegate {
    
    func onClickButton(title: String) {
        presenter.clickButton(title)
    }
    
    func selectHistory(model: HistoryModel) {
        presenter.selectHistory(model: model)
    }
    
    func math(math: String) {
        numberLabel.text =  math
        
    }
    
    func result(result: String) {
        resultLabel.text = result
    }
}
