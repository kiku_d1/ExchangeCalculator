//
//  ButtonModel.swift
//  ExchangeCalculator
//
//  Created by diwka on 1/7/22.
//

import Foundation
import UIKit

struct ButtonModel{
    var title: String
    var titleColor: UIColor
    var backgroundColor: UIColor
    
    init(_ title: String, _ titleColor: UIColor, _ backgroundColor: UIColor){
        self.title = title
        self.titleColor = titleColor
        self.backgroundColor = backgroundColor
    }
}
