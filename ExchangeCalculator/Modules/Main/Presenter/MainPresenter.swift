//
//  MainPresenter.swift
//  ExchangeCalculator
//
//  Created by diwka on 7/6/22.
//

import Foundation
import UIKit
import SnapKit
import MathExpression

protocol MainPresenterDelegate: AnyObject {
    func math(math: String)
    func result(result: String)
}

class MainPresenter{
    
    private let router: Router
    
    var delegate: MainPresenterDelegate?
    
    required init(view: MainPresenterDelegate, router: Router){
        self.delegate = view
        self.router = router
    }
    
    private var math = "0"
    private var operators = ["+", "-", "*", ".", "±", "/", "%","()"]
    private var operation = false
    
    func showExchange() {
        router.showExchange()
    }
    
    func clickButton(_ title: String) {
        
        let separatedArray = math.components(separatedBy: ["+", "-", "х", ".", "±", "÷"])
        
        if title == "="{
            return
        }
        
        if title == "%"{
            math += "*0.01*"
            calculatorMath(math)
        }else if title == "."{
            if separatedArray.last?.contains(".") == true{
                return
            }else{
                calculatemathResult(title)
            }
        } else if title == "C" {
            clearAll()
        } else if !(operators.contains(String(math.last ?? Character(""))) && operators.contains(title)){
            if title == "+" || title == "-" || title == "/" || title == "*"{
                if operation == false{
                    operation = true
                    calculatemathResult(title)
                }else if operation == true{
                    return
                }
            }else{
                operation = false
                calculatemathResult(title)
            }
        }
    }
    
    func clearLast() {
        math = String(math.dropLast())
        if math.count == 0 {
            math = "0"
        }
        calculatorMath(math)
    }
    
    func selectHistory(model: HistoryModel) {
        math = model.math
        operation = false
        delegate?.math(math: model.math)
        delegate?.result(result: model.result)
    }
    
    private func clearAll() {
        math = "0"
        delegate?.result(result: "0")
        delegate?.math(math: "0")
    }
    
    private func calculatemathResult(_ title: String) {
        if (String(math.first ?? Character(""))) == "0" {
            math = String(math.dropFirst())
        }
        math += title
        calculatorMath(math)
    }
    
    private func calculatorMath(_ math: String) {
        if !operators.contains(String(math.last ?? Character(""))) {
            let expr = NSExpression(format: math)
            let result = expr.expressionValue(with: nil, context: nil) as! Double
            let stringResult = String(result)
            let separatedArray = stringResult.components(separatedBy: ".")
            DataBase.shared.saveHistory(model: HistoryModel.createHistory(math: math, result: stringResult))
            
            if separatedArray[1] == "0"{
                delegate?.result(result: "\(separatedArray[0])")
            }else{
                delegate?.result(result: stringResult)
            }
        } else {
            delegate?.result(result: "0")
        }
        delegate?.math(math: math)
    }
}


