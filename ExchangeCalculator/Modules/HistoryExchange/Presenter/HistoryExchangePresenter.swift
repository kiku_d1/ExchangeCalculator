//
//  HistoryExchangePresenter.swift
//  ExchangeCalculator
//
//  Created by diwka on 27/6/22.
//

import Foundation
import SnapKit
import RealmSwift

protocol SharedHisstoryExchange: AnyObject{
    func showHistory(model: Results<HistoryexchangeModel>)
}

class HistoryExchangePresenter {
    
    private weak var delegate: SharedHisstoryExchange? = nil
    weak var historyDelegate: HistoryExchangeSelectDelegate? = nil
    
    func selctExchangeHistory(model: HistoryexchangeModel){
        historyDelegate?.selectExchangeHistory(model: model)
    }
    
    func deleteAllExchangeHistory(){
        DataBase.shared.deleteAllExchangeHistory()
    }
    
    func getExchangeHistory() -> Results<HistoryexchangeModel>?{
        return DataBase.shared.getExchangeHistory()
    }
}
