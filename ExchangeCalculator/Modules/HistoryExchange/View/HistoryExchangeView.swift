//
//  HistoryExchangeView.swift
//  ExchangeCalculator
//
//  Created by diwka on 27/6/22.
//

import Foundation
import SnapKit
import RealmSwift

protocol HistoryExchangeSelectDelegate: AnyObject {
    func selectExchangeHistory(model: HistoryexchangeModel)
}

class HistoryExchangeView: UIViewController {
    
    private lazy var clearButton: UIButton = {
        let view = UIButton()
        view.setTitle("Clear", for: .normal)
        view.backgroundColor = .init(named: "C")
        view.addTarget(self, action: #selector(clickClear(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var historyTable: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        view.register(HistoryEcxhangeCell.self, forCellReuseIdentifier: "HistoryEcxhangeCell")
        return view
    }()
    
    private lazy var presenter: HistoryExchangePresenter = {
        return HistoryExchangePresenter()
    }()
    
    @objc func clickClear(view: UIButton){
        DataBase.shared.deleteAllHistory()
        historyTable.reloadData()
    }
    
    private var models: Results<HistoryexchangeModel>? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        models = presenter.getExchangeHistory()
        
        view.backgroundColor = .init(named: "ColorNumber-1")
        
        historyTable.reloadData()
        
        view.addSubview(clearButton)
        clearButton.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(10)
            make.right.equalTo(view.safeArea.right).offset(-10)
            make.height.equalTo(40)
            make.width.equalTo(80)
        }
        
        view.addSubview(historyTable)
        historyTable.snp.makeConstraints { make in
            make.top.equalTo(clearButton.snp.bottom).offset(30)
            make.right.equalTo(view.safeArea.right).offset(-10)
            make.left.equalTo(view.safeArea.left).offset(10)
            make.bottom.equalTo(view.safeArea.bottom)
        }
    }
}
extension HistoryExchangeView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = historyTable.dequeueReusableCell(withIdentifier: "HistoryEcxhangeCell") as! HistoryEcxhangeCell
        let index = indexPath.row
        let model = models?[index]
        cell.fill(model: model!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
