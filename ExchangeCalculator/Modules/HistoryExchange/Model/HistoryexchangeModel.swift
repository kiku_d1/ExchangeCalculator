//
//  HistoryexchangeModel.swift
//  ExchangeCalculator
//
//  Created by diwka on 27/6/22.
//

import Foundation
import RealmSwift

class HistoryexchangeModel: Object {
    
    @objc dynamic var to = ""
    @objc dynamic var from = ""
    @objc dynamic var amount = 0
    @objc dynamic var result = 0.0
    
    static func createHistory(to: String,from: String,amount: Int, result: Double) -> HistoryexchangeModel {
        let model = HistoryexchangeModel()
        model.to = to
        model.from = from
        model.amount = amount
        model.result = result
        return model
    }
}
