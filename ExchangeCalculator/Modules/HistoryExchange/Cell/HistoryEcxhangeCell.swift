//
//  HistoryEcxhangeCell.swift
//  ExchangeCalculator
//
//  Created by diwka on 27/6/22.
//

import Foundation
import UIKit
import SnapKit

class HistoryEcxhangeCell: UITableViewCell {
    
    private lazy var fromCurrencyLabel: UILabel = {
        let view =  UILabel()
        view.text = "55"
        view.font = .systemFont(ofSize: 28, weight: .regular)
        view.textAlignment = .right
        return view
    }()
    
    private lazy var toCurrencyLabel: UILabel = {
        let view =  UILabel()
        view.text = "55"
        view.font = .systemFont(ofSize: 28, weight: .regular)
        view.textAlignment = .right
        return view
    }()
    
    private lazy var amountCurrencyLabel: UILabel = {
        let view = UILabel()
        view.text = "55"
        view.font = .systemFont(ofSize: 28, weight: .regular)
        view.textAlignment = .right
        return view
    }()
    
    private lazy var resultExchange: UILabel = {
        let view = UILabel()
        view.text = ""
        view.font = .systemFont(ofSize: 28, weight: .regular)
        view.textAlignment = .right
        view.textColor = .init(named: "ColorLine")
        return view
    }()
    
    override func layoutSubviews() {
        
        addSubview(toCurrencyLabel)
        toCurrencyLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-19)
        }
        addSubview(amountCurrencyLabel)
        amountCurrencyLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.right.equalTo(toCurrencyLabel.snp.left).offset(-10)
        }
        addSubview(fromCurrencyLabel)
        fromCurrencyLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.right.equalTo(amountCurrencyLabel.snp.left).offset(-10)
        }
        addSubview(resultExchange)
        resultExchange.snp.makeConstraints { make in
            make.top.equalTo(fromCurrencyLabel.snp.bottom).offset(20)
            make.right.equalToSuperview().offset(-19)
            make.left.equalToSuperview()
            make.width.equalTo(48)
        }
    }
    
    func fill(model: HistoryexchangeModel){
        fromCurrencyLabel.text = model.from
        amountCurrencyLabel.text = "\(model.amount)"
        toCurrencyLabel.text = model.to
        resultExchange.text = "\(model.result)"
    }
}
