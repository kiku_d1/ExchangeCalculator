//
//  CustomButton.swift
//  ExchangeCalculator
//
//  Created by diwka on 1/7/22.
//

import Foundation
import UIKit

class CustomButton: UIButton {
    
    private var onClick: (CustomButton) -> Void = { _ in }
    
    init(title: String){
        super.init(frame: .zero)
        
        setTitle(title, for: .normal)
        setTitleColor(.white, for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: 36, weight: .semibold)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setOnCLick(onCLick: @escaping (CustomButton) -> Void) {
        self.onClick = onCLick
        
        addTarget(self, action: #selector(clickButton(view:)), for: .touchUpInside)
    }
    
    @objc func clickButton(view: UIButton) {
        onClick(self)
    }
}
