//
//  UIButtonLayoutView.swift
//  ExchangeCalculator
//
//  Created by diwka on 1/7/22.
//

import Foundation
import UIKit

protocol UIButtonLayoutDelegate: AnyObject {
    func onClickButton(title: String)
}

class UIButtonLayoutView: UIView {
    
    private lazy var horizontalSteck: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fillEqually
        return view
    }()
    
    public weak var delegate: UIButtonLayoutDelegate? = nil
    
    private let buttons = [
        [ButtonModel("C", UIColor.init(named: "ColorNumber-1")!, UIColor.init(named: "C")!),
         ButtonModel("()", UIColor.init(named: "ColorOperation")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("%", UIColor.init(named: "ColorOperation")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("/", UIColor.init(named: "ColorOperation")!, UIColor.init(named: "BackroundColor")!)],
        
        [ButtonModel("7", UIColor.init(named: "ColorNumber")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("8", UIColor.init(named: "ColorNumber")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("9", UIColor.init(named: "ColorNumber")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("*", UIColor.init(named: "ColorOperation")!, UIColor.init(named: "BackroundColor")!)],
        
        [ButtonModel("4", UIColor.init(named: "ColorNumber")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("5", UIColor.init(named: "ColorNumber")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("6", UIColor.init(named: "ColorNumber")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("-", UIColor.init(named: "ColorOperation")!, UIColor.init(named: "BackroundColor")!),],
        
        [ButtonModel("1", UIColor.init(named: "ColorNumber")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("2", UIColor.init(named: "ColorNumber")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("3", UIColor.init(named: "ColorNumber")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("+", UIColor.init(named: "ColorOperation")!, UIColor.init(named: "BackroundColor")!),],
        
        [ButtonModel("±", UIColor.init(named: "ColorNumber")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("0", UIColor.init(named: "ColorNumber")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel(".", UIColor.init(named: "ColorNumber")!, UIColor.init(named: "BackroundColor")!),
         ButtonModel("=", UIColor.init(named: "ColorNumber-1")!, UIColor.init(named: "ColorOperation")!)],
    ]
    
    private func seconStart(_ cornerRadius: Double){
        buttons.forEach { butonnsItem in
            let stack = creatStack()
            
            butonnsItem.forEach { item in
                stack.addArrangedSubview(createButton(model: item, cornerRadius))
            }
            self.horizontalSteck.addArrangedSubview(stack)
        }
    }
    
    private func creatStack() -> UIStackView {
        let view = UIStackView()
        view.axis = .horizontal
        view.spacing = 15
        view.distribution = .fillEqually
        return view
    }
    
    func createButtonLayouts(cornerRadius: Double) {
        horizontalSteck.spacing = 15
        addSubview(horizontalSteck)
        horizontalSteck.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(5)
            make.left.equalToSuperview().offset(5)
            make.right.equalToSuperview().offset(-5)
            make.bottom.equalToSuperview().offset(-5)
            
        }
        seconStart(cornerRadius)
    }
    
    private func createButton(model: ButtonModel, _ cornerRadius: Double) -> UIButton {
        let view = UIButton(type: .system)
        view.addTarget(self, action: #selector(clickButtons(view:)), for: .touchUpInside)
        view.titleLabel?.font = UIFont.systemFont(ofSize: 38, weight: .semibold)
        view.setTitle(model.title, for: .normal)
        view.backgroundColor = model.backgroundColor
        view.setTitleColor(model.titleColor, for: .normal)
        view.layer.shadowRadius = 10
        view.layer.shadowOpacity = 0.05
        view.layer.shadowColor = UIColor.white.cgColor
        
        DispatchQueue.main.async {
            view.layer.cornerRadius = view.frame.width / 9
        }
        return view
    }
    
    @objc func clickButtons(view: UIButton) {
        delegate?.onClickButton(title: view.titleLabel?.text ?? String())
    }
}
