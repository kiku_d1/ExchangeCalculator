//
//  ModuleBuilder.swift
//  ExchangeCalculator
//
//  Created by diwka on 28/6/22.
//

import SnapKit
import Foundation

class ModuleBuilder {
    
    func createCalculatorModule(router: Router) -> UIViewController {
        let view = MainView()
        let presenter = MainPresenter(view: view, router: router)
        
        view.presenter = presenter
        
        return view
    }
    
    func createExchangeModule(router: Router) -> UIViewController {
        let view = ExchangeView()
        let presenter = ExchangePresenter(view: view, router: router)
        
        view.presenter = presenter
        
        return view
    }
}
