//
//  Router.swift
//  ExchangeCalculator
//
//  Created by diwka on 28/6/22.
//

import Foundation
import UIKit

class Router {
        
        private let moduleBuilder: ModuleBuilder
        private let navigationController: UINavigationController
        
        init(moduleBuilder: ModuleBuilder, navigationController: UINavigationController) {
            self.navigationController = navigationController
            self.moduleBuilder = moduleBuilder
        }
        
        func initMainModule() {
            let module = moduleBuilder.createCalculatorModule(router: self)
            
            navigationController.viewControllers = [module]
        }
        
        func showExchange() {
            let module = moduleBuilder.createExchangeModule(router: self)
            
            navigationController.pushViewController(module, animated: true)
        }
        
        func back() {
            navigationController.popViewController(animated: true)
        }
    }
