//
//  BaseNavigationCintroller.swift
//  ExchangeCalculator
//
//  Created by diwka on 29/6/22.
//

import Foundation
import SnapKit
class BaseNavigationController: UINavigationController {
    
    override func viewDidAppear(_ animated: Bool) {
        isNavigationBarHidden = true
    }
}
