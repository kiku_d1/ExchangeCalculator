//
//  DataBase.swift
//  ExchangeCalculator
//
//  Created by diwka on 7/6/22.
//

import Foundation
import RealmSwift

class DataBase {
    
    static var shared = DataBase()
    
    private lazy var realm: Realm? = {
        do {
            return try Realm()
        } catch {
            return nil
        }
    }()
    
    func saveHistory(model: HistoryModel) {
        do {
            try realm?.write {
                realm?.add(model)
                dump(model)
            }
        } catch {
            print("DataBase not work saveHistory")
        }
    }
    
    func getHistory() -> Results<HistoryModel>? {
        return realm?.objects(HistoryModel.self)
    }
    
    func deleteAllHistory() {
        do {
            try realm?.write {
                realm?.deleteAll()
            }
        } catch {
            print("DataBase not work saveHistory") 
        }
    }
    
    func saveHistoryExchange(model: HistoryexchangeModel) {
        do {
            try realm?.write {
                realm?.add(model)
                dump(model)
            }
        }catch {
            print("Database not work saveExchangeHistory")
        }
    }
    
    func getExchangeHistory() -> Results<HistoryexchangeModel>?{
        return realm?.objects(HistoryexchangeModel.self)
    }
    
    func deleteAllExchangeHistory(){
        do {
            try realm?.write {
                realm?.deleteAll()
            }
        }catch {
            print("Database not work saveExchangeHistory")
        }
    }
}
