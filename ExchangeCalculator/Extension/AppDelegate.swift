//
//  AppDelegate.swift
//  ExchangeCalculator
//
//  Created by diwka on 7/6/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        initNavigations()
        
        return true
    }
    
    func initNavigations() {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let navigationController = BaseNavigationController()
        let moduleBuilder = ModuleBuilder()
        let router = Router(moduleBuilder: moduleBuilder, navigationController: navigationController)
        
        router.initMainModule()
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

